package com.beats.userdetails.controller;

import com.beats.userdetails.model.User;
import com.beats.userdetails.service.UserService;
import lombok.RequiredArgsConstructor;
import org.reactivestreams.Publisher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class UserController {
  private final UserService service;

  @GetMapping("test")
  public Publisher<User> getAll() {
    return service.getAll();
  }
}
